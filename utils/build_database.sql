-- !preview conn=DBI::dbConnect(RSQLite::SQLite())

/* Run script on MySql */
/* mysql -u user -p < C:\Users\User\Desktop\filename.sql */


/* Create data base */
CREATE DATABASE IF NOT EXISTS btc;
use btc;

/* Create tables */
/* Stores daily bitcoin data for all data sources.
    The variable 'volume' can represent different things depending on the data source, however,
    the same data type is used for storage (double) */
CREATE TABLE IF NOT EXISTS daily (
    source VARCHAR(255) NOT NULL,
    ticker VARCHAR(255) NOT NULL,
    time DATE,
    open DOUBLE NOT NULL,
    high DOUBLE NOT NULL,
    low DOUBLE NOT NULL,
    close DOUBLE NOT NULL,
    volume DOUBLE,
    primary key (source, ticker, time)
);

/* Setup privilegies */
CREATE USER 'linux_user'@'localhost' IDENTIFIED BY 'password_to_mysql_user';
GRANT ALL PRIVILEGES ON btc.* TO 'root'@'localhost';
GRANT ALL PRIVILEGES ON btc.* TO 'linux_user'@'localhost';
FLUSH PRIVILEGES;

/* Test database */
INSERT INTO daily (source, ticker, time, open, high, low, close, volume)
VALUES ("source1", "BTC-USD", "1970-01-01", 10, 20, 5, 15, 111999777666); 

SELECT * FROM daily;

/* Clean table */
 DELETE FROM daily;
 
 