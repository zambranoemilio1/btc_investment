&nbsp;&nbsp;

## **Introduction**

This document contains important information about the setup and operation of the Bitcoin analysis system.


## **Indice**
1. General objective  
2. Prerequisites  
    2.1. MySQL  
    2.2. Hadoop/MapReduce  
3. Architecture  
4. Technical requirements  
5. Running system
6. Possible improvements

**Note:** It is recommend that you open this README in another tab as you perform the tasks below. 


---

## **1. General objective**

The system aims to be a base for the development of Bitcoin analysis projects using the integration of different analysis and data storage-processing strategies.

---

## **2. Prerequisites**

For the project to work it is necessary to install and configure the MySQL database management system and the Hadoop/MapReduce Big Data framework.

### **2.1. MySQL**

In order for the interaction with the database to work, the following must be done:

1. Create the database and its tables.    
  For this, a MySQL script called 'build_database.sql' located in the 'utils' folder is included in the project.

2. User and password settings.  
  To be able to connect from R to the database it is necessary to establish the connection parameters, for this, we must modify the user and the password in the files 'concentrator_daily.R' and 'btc_investment.Rmd' located in the folders 'etl' and 'visualization' respectively.

**Note: ** It is recommended to first test the connection to the database separately in an R script to verify that everything is working correctly.


### **2.2. Hadoop/MapReduce**

The Hadoop/MapReduce installation can be done in the desired location on the operating system. However, within the system, the Hadoop/MapReduce environment variables must be changed to match the user's installation.

The modification must be made in the files 'concentrator_daily.R' and 'pattern_probability_map_reduce.R' located in the folders 'etl' and 'processing' respectively.

**Note: ** It is recommended first to test separately the interaction with Hadoop/MapReduce in an R script to verify that everything is working correctly.

---

## **3. Architecture**

The system is designed in a modular way, where the output of each module is used as the input of the next.

The order of execution of the modules is as follows:

1. etl: in charge of extracting the data from the sources and storing them in the database and HDFS.
2. processing: takes care of the calculations in Hadoop/MapReduce.
3. visualization: generate the dashboards.

The technical decision to use this architecture is based on the scalability of the system, since the modular division allows the subsequent deployment of the system in services.

---

## **4. Technical requirements**

The system has been developed and tested using the following technologies:

* Operating system: Ubuntu >= 20.04
* MySQL: version 8.0.22-0
* Hadoop/MapREduce: version 3.3.0
* R: version 4.0.3

---

## **5. Running system**
To run the system we must first have everything correctly configured and tested.

The execution of the modules is done by running the script 'btc_investment.R'.

**Note: ** it is important that the Hadoop/MapReduce modules are running. To do this, you can verify that the processes are active using the 'jps' command, whose output should be similar to:

9136 Jps  
8530 SecondaryNameNode  
8338 DataNode  
8210 NameNode  
8778 ResourceManager  
8971 NodeManager  

The result of a successful execution should generate a dashboard like the one shown in the following images.

![BTC Short term](images/btc_short_term.png)
![BTC Medium term](images/btc_medium_term.png)
![BTC Odds](images/btc_odds.png)
---

## **6. Possible improvements**  

Some ideas are presented on possible improvements to the system to be deployed in a production environment.

* Improve the fault tolerance of the system. Add installation check and configuration of system prerequisites.
* Migrate some calculations that are made in the visualization module to the processing module.
* Create a general configuration file for the entire project and a configuration file for each module.
* Allow modules to run independently as web services.
* Add more indicators.

&nbsp;&nbsp;&nbsp;&nbsp;