# This script run all concentrators contained in "concentrators" folder
# The objective of the Concentrator is to collect all scrapped data and store them in a
# historical database as well as in Hadoop MapReduce for further processing.

# Get path of concentrators
path <- "etl/concentrators/"

# Get file names
file_names <- dir(path)

# Run all concentrators
for (i in 1:length(file_names)) {
  file <- paste(path, file_names[i], sep = "")
  command <- paste("Rscript", file, sep = " ")
  system(command)
}

# Verify all concentrators are successful
tmp_files <- dir("data/tmp")
if (length(tmp_files) != 0) {
  stop("Error - concentrators not running successful")
} else {
  message("Concentrator process successful!")
}
